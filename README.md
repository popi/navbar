Multisite navbar
================

This project generates an HTML navigation menu from a JSON file located at a specified URL.

The URL must be set in `nav.js` (`ynh_url` variable).

For presentation and quick howto, please consult https://foss-notes.blog.nomagic.uk/

## Tips

The json file can be generated automatically from a yaml file, if your solution allows it.

- Example with Jekyll:

Content of file nav.html
```
---
layout: none
permalink: /third_party/simple-nav/nav.json
---
{{ site.data.navbar | jsonify }}

```

This will take the content of yaml file `mysite/_data/navbar.yaml` and convert it into json, with the json file available at `http(s)://mysite/third_party/simple-nav/nav.json`.

